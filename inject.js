/* Constants */
const IMAGE_PROPERTIES_DIALOG = "image-properties",
	BOOKMARK_OPTIONS_DIALOG = "bookmark-options";

/* Generators */
var dialog = (function() {
	var cover = document.createElement("div"),
		dialog = document.createElement("dialog"),
		body = document.querySelector("body"),
		imageProperties = [
						{key: "image-name", title: "Image"},
						{key: "dimensions", title: "Dimensions"},
						{key: "camera-settings", title: "Camera settings"},
						{key: "image-type", title: "Image type"},
						{key: "file-size", title: "File size"},
						{key: "address", title: "Address"},
						{key: "alternative-text", title: "Alternative text"},
						{key: "long-description", title: "Long description"}
					 ],
		bookmarkProperties = [
			{key: "title", title: "Name"},
			{key: "folders", title: "Create in"},
			{key: "url", title: "Address"}
		];

	function show(url, dialogType) {
		// insert info
		beforeDialogDisplay(url, dialogType);

		// add close button
		var button = document.createElement("button");
		button.setAttribute("type", "button");
		button.name = "dialog-close";
		button.addEventListener("click", function() {
			hide(false);
		}, false);
		button.textContent = "Close";
		dialog.appendChild(button);

		// show
		dialog.className = "classic-context-menu-dialog";
		if (!dialog.parentElement) {
			// first time run, attach events, etc
			dialog.addEventListener("close", function() {
				hide(true);
			}, false);

			// append to page
			body.appendChild(dialog);
		}

		dialog.showModal();

		afterDialogDisplay(url, dialogType);
	};

	function hide(fromEvent) {
		if (!fromEvent) dialog.close();

		while (dialog.lastChild) {
			dialog.removeChild(dialog.lastChild);
		}
	};

	function beforeDialogDisplay(url, dialogType) {
		if (dialogType === IMAGE_PROPERTIES_DIALOG) {
			var list = document.createElement("dl"), imageElement, width, height;

			// Brute force matching on img src by the url we got from background
			url = url.replace(/^https?:/, '');
			while (url.indexOf("/") >= 0) {
				imageElement = document.querySelector("img[src$='" + url + "']");
				if (!imageElement) {
					// match: most specific to least.
					url = url.replace(/.*?\//,'');
				} else {
					break;
				}
			}

			width = imageElement.naturalWidth || imageElement.width;
			height = imageElement.naturalHeight || imageElement.height;

			list.className = "image-properties-list";

			imageProperties.forEach(function(value, index) {
				var term = document.createElement("dt"),
					definition = document.createElement("dd");

				term.textContent = value.title;

				if (value.key === "image-name") {
					definition.textContent = url.substring(url.lastIndexOf("/") + 1);
				} else if (value.key === "address") {
					definition.textContent = url;
				} else if (value.key === "dimensions") {
					definition.textContent = width + " x " + height + " pixels";
				} else if (value.key === "alternative-text") {
					definition.textContent = imageElement.alt;
				} else {
					definition.textContent = "";
				}

				definition.className = value.key;

				list.appendChild(term);
				list.appendChild(definition);
			});
			dialog.appendChild(list);
		} else if (dialogType === BOOKMARK_OPTIONS_DIALOG) {
			var list = document.createElement("dl"),
				anchors = document.querySelectorAll("a"),
				button = document.createElement("button"),
				anchor = null, defaultTitle, i = 0;

			// find title
			for (; i < anchors.length; i++) {
				if (anchors[i].href.indexOf(url) >= 0) {
					anchor = anchors[i];
					break;
				}
			}

			defaultTitle = anchor ? anchor.textContent || anchor.title : document.title;

			button.setAttribute("type", "button");
			button.name = "save-bookmark";
			button.textContent = "Add Bookmark";
			button.addEventListener("click", function() {
				var url = dialog.querySelector("input[name=url]").value,
					title = dialog.querySelector("input[name=title]").value,
					parentId = dialog.querySelector("select[name=parent-id]").value;
				chrome.runtime.sendMessage(null, {action: "create-bookmark", payload: {url: url, title: title, parentId: parentId}}, function() {
					hide();
				});
			}, false);

			list.className = "bookmark-properties-list";

			bookmarkProperties.forEach(function(value, index) {
				var term = document.createElement("dt"),
					definition = document.createElement("dd"),
					userInput;

				term.textContent = value.title;

				if (value.key === "title") {
					userInput = document.createElement("input");
					userInput.type = "text";
					userInput.name = value.key;
					userInput.value = defaultTitle;
					definition.appendChild(userInput);
				} else if (value.key === "url") {
					userInput = document.createElement("input");
					userInput.type = "text";
					userInput.name = value.key;
					userInput.value = url;
					definition.appendChild(userInput);
				} else if (value.key === "folders") {
					definition.textContent = "loading...";
				}

				definition.className = value.key;
				
				list.appendChild(term);
				list.appendChild(definition);
			});

			dialog.appendChild(list);
			dialog.appendChild(button);
		}
	};

	function afterDialogDisplay(url, dialogType) {
		var connection;
		if (dialogType === IMAGE_PROPERTIES_DIALOG) {
			connection = ConnectionFactory.imageWorker();
			connection.postMessage({action: "image-analyze", payload: url});
			connection.onMessage.addListener(parseImageWorkerData);
		} else if (dialogType === BOOKMARK_OPTIONS_DIALOG) {
			// get folders
			connection = ConnectionFactory.bookmarkWorker();
			connection.postMessage({action: "get-folders", payload: null});
			connection.onMessage.addListener(parseBookmarkWorkerData);
		}
	};

	function parseBookmarkWorkerData(event) {
		if (event.action && event.action.length) {
			if (event.action === "folders" && event.payload) {
				console.log(event.payload);
				if (event.payload.length) {
					var element = document.createElement("select"),
						folders = dialog.querySelector("dd.folders");
					// Opera has bookmark bar and other bookmarks
					// Bookmark has to be within one, the root doesn't seem to allow for bookmarks
					event.payload[0].forEach(function(value, index) {
						parseBookmarkTree(value, element);
					});
					element.name = "parent-id";
					folders.textContent = "";
					folders.appendChild(element);
				}
			}
		}
	};

	function parseBookmarkTree(tree, element, level) {
		level = level || 0;
		var prefix = "";
		for (var i = 0; i < level; i++) {
			prefix += "-";
		}
		var option = document.createElement("option");
			option.value = tree.id;
			option.text = prefix + tree.name;

			element.appendChild(option);
		if (tree.subFolders && tree.subFolders.length) {
			level++;
			tree.subFolders.forEach(function(value, index) {
				parseBookmarkTree(value, element, level);
			});
		}
	};

	function parseImageWorkerData(event) {
		if (event.action && event.action.length && event.payload) {
			if (event.action === "basic-data") {
				dialog.querySelector("dd.image-type").textContent = event.payload.imageType;
				dialog.querySelector("dd.file-size").textContent = totalBytesToSimpleString(event.payload.fileSize);
			} else if (event.action === "exif") {
				addExifData(dialog.querySelector("dd.long-description"), dialog.querySelector("dd.camera-settings"), dialog.querySelector("dd.dimensions"), event.payload);
			} else if (event.action === "png-data") {
				console.log("png-data");
				addExifData(dialog.querySelector("dd.long-description"), null, dialog.querySelector("dd.dimensions"), event.payload);
			}
		}
	};

	function totalBytesToSimpleString(bytes) {
		var prefix = ["", "KB", "MB", "GB"], // a TB image would be ridiculous. So would GB.
			base = 1000,
			power = prefix.length - 1,
			breakpoint;

		while (power >= 0) {
			breakpoint = Math.pow(base, power);
			if (breakpoint <= bytes) {
				return (bytes / breakpoint).toFixed(2) + " " + prefix[power];
			}
			power--;
		}

		return bytes.toFixed(2) + " Bytes";
	};

	function addExifData(exifElement, settingsElement, dimensionsElement, tags) {
		var list = document.createElement("ul");
		for (var name in tags) {
			if (!/^undefined/i.test(name) && !/^unknown/i.test(name) && tags[name] && tags[name].hasOwnProperty("description")) {
				(function() {
					var item = document.createElement("li");
					item.innerHTML = "<strong>" + name + ":</strong> " + tags[name].description;
					list.appendChild(item);
				})();
			}
		}

		if (list.hasChildNodes()) {
			exifElement.appendChild(list);
		}

		if (settingsElement) {
			var settings = "";
			
			if (tags["FocalLength"]) {
				settings += tags["FocalLength"].description + "mm, ";
			}
			if (tags["FNumber"]) {
				settings += "f/" + tags["FNumber"].description + ", ";
			}
			if (tags["ExposureTime"]) {
				settings += tags["ExposureTime"].description.toFixed(5) + "s ";
			}
			if (tags["ISOSpeedRatings"]) {
				settings += "@ ISO " + tags["ISOSpeedRatings"].description + ", ";
			}
			if (tags["Model"]) {
				settings += tags["Model"].description;
			}

			settingsElement.textContent = settings;
		}

		if (tags.bitsPerPixel) {
			dimensionsElement.textContent += " @ " + tags.bitsPerPixel + " bits per pixel";
		}

		if (tags.frameCount) {
			dimensionsElement.textContent += " animated in " + tags.frameCount + " frames";
		}
	};

	return {
		showImageProperties: function(url) { show(url, IMAGE_PROPERTIES_DIALOG); },
		showBookmarkOptions: function(url) { show(url, BOOKMARK_OPTIONS_DIALOG); },
		hide: hide
	};
})();


var ConnectionFactory = (function() {

	function create(name) {
		return chrome.runtime.connect({name: name});
	};

	return {
		imageWorker: function() {return create("image-worker");},
		bookmarkWorker: function() {return create("bookmark-worker");}
	};
})();

/* Runtime listeners */
chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
	if (message.action === "reload-image") {
		reloadImage(message.payload);
	} else if (message.action === "inspect-image") {
		dialog.showImageProperties(message.payload);
	} else if (message.action === "bookmark-link") {
		dialog.showBookmarkOptions(message.payload);
	} else {
		sendResponse({result: "error", message: message});
		return;
	}

	sendResponse({result: "success"});
});


/* Utility functions */
function reloadImage(url) {
	var i = 0,
		images = document.querySelectorAll("img[src$='" + url.replace(/^https?:/, '') + "']");

	function forceReload(value, index) {
		var cachebust = "classiccontextmenu_t=" + (new Date()).getTime();
		if (value.src.indexOf("?") >= 0) {
			value.src += "&" + cachebust;
		} else {
			value.src += "?" + cachebust;
		}
	};

	for (; i < images.length; i++) {
		forceReload(images[i], i);
	}
};