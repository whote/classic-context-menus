/* Tab event listeners */
chrome.tabs.onUpdated.addListener(function(id, info, tab) {
    if (info.url && info.url.length) {
        removeInjectionStatus(id);
    }
});

chrome.tabs.onRemoved.addListener(function(removedTabID, removeInfo) {
    removeInjectionStatus(removedTabID);
});

chrome.tabs.onReplaced.addListener(function(addedTabID, removedTabID) {
    removeInjectionStatus(removedTabID);
});

/* Runtime listeners */
chrome.runtime.onInstalled.addListener(function() {
    Console.log("installed");
     cleanLocalStorage();
});

chrome.runtime.onStartup.addListener(function() {
    Console.log("startup");
     cleanLocalStorage();
});

function openUrl(url, withFocus, inBackground) {
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function(tabs) {
        if (tabs.length) {
            var currentTab = tabs[0];

            if (!inBackground) {
                chrome.tabs.update(null, {
                    url: url,
                    active: withFocus
                });
            } else {
                chrome.tabs.create({
                    active: withFocus,
                    index: currentTab.index + 1,
                    openerTabId: currentTab.id,
                    url: url
                });
            }
        }
    });
};

function inject(message) {
    function sendMessage(id) {
        chrome.tabs.sendMessage(id, message, function(response) {
            Console.log(response);
        });
    };

    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function(tabs) {
        if (tabs.length) {
            var currentTab = tabs[0];

            chrome.storage.local.get({tabInjectionStatus: {}}, function(value) {
                var key = TAB_INJECTION_KEY + currentTab.id,
                    status = value.tabInjectionStatus.hasOwnProperty(key) ? value.tabInjectionStatus[key] : {id: currentTab.id, injected: false};

                if (status.injected === true) {
                    sendMessage(currentTab.id);
                } else {
                    value.tabInjectionStatus[key] = {id: currentTab.id, injected: true};

                    chrome.storage.local.set({tabInjectionStatus: value.tabInjectionStatus});
                    chrome.tabs.insertCSS(currentTab.id, {file: "inject.css"}, function() {
                        chrome.tabs.executeScript(currentTab.id, {file: "inject.js"}, function() {
                            sendMessage(currentTab.id);
                        });
                    });
                }
            });
        }
    });
};

function removeInjectionStatus(id) {
    var key = TAB_INJECTION_KEY + id;
    chrome.storage.local.get({tabInjectionStatus: {}}, function(data) {
        if (data.tabInjectionStatus.hasOwnProperty(key)) {
            delete data.tabInjectionStatus[key];
            chrome.storage.local.set({tabInjectionStatus: data.tabInjectionStatus});
        }
    });
};

function cleanLocalStorage() {
    // Get all
    chrome.storage.local.get({tabInjectionStatus: {}}, function (items) {
        // Remove tab-injection stuff
        Object.keys(items.tabInjectionStatus).forEach(function(value) {
            delete items.tabInjectionStatus[value];
        });
        chrome.storage.local.set({tabInjectionStatus: items.tabInjectionStatus});
    });
};

/* Polyfill, I just want the convience of using a map */
// Production steps of ECMA-262, Edition 6, 22.1.2.1
// Reference: https://people.mozilla.org/~jorendorff/es6-draft.html#sec-array.from
if (!Array.from) {
  Array.from = (function () {
    var toStr = Object.prototype.toString;
    var isCallable = function (fn) {
      return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
    };
    var toInteger = function (value) {
      var number = Number(value);
      if (isNaN(number)) { return 0; }
      if (number === 0 || !isFinite(number)) { return number; }
      return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
    };
    var maxSafeInteger = Math.pow(2, 53) - 1;
    var toLength = function (value) {
      var len = toInteger(value);
      return Math.min(Math.max(len, 0), maxSafeInteger);
    };

    // The length property of the from method is 1.
    return function from(arrayLike/*, mapFn, thisArg */) {
      // 1. Let C be the this value.
      var C = this;

      // 2. Let items be ToObject(arrayLike).
      var items = Object(arrayLike);

      // 3. ReturnIfAbrupt(items).
      if (arrayLike == null) {
        throw new TypeError("Array.from requires an array-like object - not null or undefined");
      }

      // 4. If mapfn is undefined, then let mapping be false.
      var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
      var T;
      if (typeof mapFn !== 'undefined') {
        // 5. else      
        // 5. a If IsCallable(mapfn) is false, throw a TypeError exception.
        if (!isCallable(mapFn)) {
          throw new TypeError('Array.from: when provided, the second argument must be a function');
        }

        // 5. b. If thisArg was supplied, let T be thisArg; else let T be undefined.
        if (arguments.length > 2) {
          T = arguments[2];
        }
      }

      // 10. Let lenValue be Get(items, "length").
      // 11. Let len be ToLength(lenValue).
      var len = toLength(items.length || items.size);

      // 13. If IsConstructor(C) is true, then
      // 13. a. Let A be the result of calling the [[Construct]] internal method of C with an argument list containing the single item len.
      // 14. a. Else, Let A be ArrayCreate(len).
      var A = isCallable(C) ? Object(new C(len)) : new Array(len);

      // 16. Let k be 0.
      var k = 0;
      var entries = items.constructor.prototype.hasOwnProperty("entries") && typeof(items.entries) === "function" ? items.entries() : undefined;
      // 17. Repeat, while k < len… (also steps a - h)
      var kValue;
      while (k < len) {
        kValue = entries ? entries.next().value : items[k];
        if (mapFn) {
          A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
        } else {
          A[k] = kValue;
        }
        k += 1;
      }
      // 18. Let putStatus be Put(A, "length", len, true).
      A.length = len;
      // 20. Return A.
      return A;
    };
  }());
}