const Console = (function() {
	"use strict";

	var isDebug = false;

/* Out for now - no general logging unless manually overriden.
	chrome.management.getSelf(function(info) {
		isDebug = info.installType === "development";
	}); */

	function callConsole(method) {
		if (!isDebug) return;

		var args = [], count = arguments.length;
		for (let i = 1; i < count; i++) {
			args[i-1] = arguments[i];
		}

		console[method].apply(console, args);
	};

	function log() {
		if (!isDebug) return;

		var args = ["log"], count = arguments.length;
		for (let i = 0; i < count; i++) {
			args[i+1] = arguments[i];
		}

		callConsole.apply(this, args);
	};

	return {
		call: callConsole,
		log: log,
		debug: log,
		info: log
	};
})();