chrome.runtime.onConnect.addListener(function(port) {
	if (port.name === "bookmark-worker") {
		port.onMessage.addListener(function(message) {
			if (message.action && message.action.length) {
				if (message.action === "get-folders") {
					chrome.bookmarks.getTree(function(results) {
						var output = getBookmarkFolders(results);
						port.postMessage({action: "folders", payload: output});
					});
				} else if (message.action === "get-folder-contents" && message.payload) {
					chrome.bookmarks.get(message.payload, function(results) {
						port.postMessage({action: "folder-contents", payload: results});
					});
				} else if (message.action === "create" && message.payload) {
					chrome.bookmarks.create(message.payload, function(result) {
						port.postMessage({action: "complete", payload: result});
					});
				} else if (message.action === "get-all") {
					if (message.payload) {
						chrome.bookmarks.getSubTree(message.payload, function(results) {
							port.postMessage({action: "bookmarks", payload: results});
						});
					} else {
						chrome.bookmarks.getTree(function(results) {
							port.postMessage({action: "bookmarks", payload: results});
						});
					}
				} else if (message.action === "delete-bookmark" && message.payload) {
					chrome.bookmarks.remove(message.payload);
				} else {
					Console.log("error in bookmark worker: ", message.action, message.payload);
				}
			}
		});
	}
});

chrome.runtime.onMessage.addListener(function(message, sender, sendResponse) {
	Console.log(message, sender, sendResponse);
	if (message.action && message.action.length) {
		if (message.action === "create-bookmark" && message.payload) {
			chrome.bookmarks.create(message.payload);
		} else if (message.action === "track-tab-info") {
			// ??
		}
	}

	sendResponse();
});

function getBookmarkFolders(tree) {
	var output = [];
	if (tree.length && tree[0].children) {
		output.push(parseSubTree(tree[0].children));
		/*tree[0].children.forEach(function(value, index) {
			if (value.children) {
				var subTree = parseSubTree(value.children);
				output.push(subTree);
			}
		});*/
	}

	return output;
};

function parseSubTree(subTree) {
	var output = [];
	if (subTree.length) {
		subTree.forEach(function(value, index) {
			if (value.children && value.children.length) {
				output.push({name: value.title, id: value.id, subFolders: parseSubTree(value.children) || null});
			}
		});
	}
	return output;
};