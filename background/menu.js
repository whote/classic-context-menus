
/*
TODO:
	- let within functions, use strict required
	- use strict the whole damn thing?
*/

Console.log("menu.js loaded!");

/* Insert menu items */
function insertContextMenuItems() {
	if (contextMenuCreated) return;

	contextMenuCreated = true;

	chrome.contextMenus.removeAll(function() {
		menuItems.forEach(function(value, index) {
			chrome.contextMenus.create({
				id: value.id,
				contexts: value.contexts,
				title: value.title,
				parentId: value.parentId,
				type: value.type,
				checked: value.checked
			});
		});
	});
};

/* Runtime listeners */
chrome.runtime.onInstalled.addListener(function() {
	Console.log("installed");
	contextMenuCreated = false;
	insertContextMenuItems();
});

chrome.runtime.onStartup.addListener(function() {
	Console.log("startup");
	contextMenuCreated = false;
	insertContextMenuItems();
});

/* Menu item click listener */
chrome.contextMenus.onClicked.addListener(function(info, tab) {

	if (info.menuItemId === linkOpenInBackgroundTabMenuItemID) {
		if (info.linkUrl && info.linkUrl.length) {
			openUrl(info.linkUrl, false, true);
		}
	} else if (info.menuItemId === linkBookmarkLinkMenuItemID) {
		if (info.linkUrl && info.linkUrl.length) {
			// inject content script, send message with URL to get additional details? specify bookmark details?
			chrome.bookmarks.create({
				title: info.linkUrl,
				url: info.linkUrl,
				parentId: BOOKMARKS_BAR_ID
			}, function(result) {
				Console.log(result);
			});
		}
	} else if (info.menuItemId === linkBookmarkLinkDialogMenuItemID) {
		if (info.linkUrl && info.linkUrl.length) {
			inject({action: "bookmark-link", payload: info.linkUrl});
		}
	} else if (info.menuItemId === pageBookmarkPageMenuItemID) {
		if (info.pageUrl && info.pageUrl.length) {
			inject({action: "bookmark-link", payload: info.pageUrl});
		}
	} else if (info.menuItemId === linkOpenInCurrentTabMenuItemID) {
		if (info.linkUrl && info.linkUrl.length) {
			openUrl(info.linkUrl, true, false);
		}
	} else if (info.menuItemId === linkOpenInNewTabMenuItemID) {
		if (info.linkUrl && info.linkUrl.length) {
			openUrl(info.linkUrl, true, true);
		}
	} else if (info.menuItemId === frameOpenInCurrentTabMenuItemID) {
		if (info.frameUrl && info.frameUrl.length) {
			openUrl(info.frameUrl, true, false);
		}
	} else if (info.menuItemId === frameOpenInNewTabMenuItemID) {
		if (info.frameUrl && info.frameUrl.length) {
			openUrl(info.frameUrl, true, true);
		}
	} else if (info.menuItemId === frameOpenInBackgroundTabMenuItemID) {
		if (info.frameUrl && info.frameUrl.length) {
			openUrl(info.frameUrl, false, true);
		}
	} else if (info.menuItemId === imageOpenImageInCurrentTabMenuItemID) {
		if (info.srcUrl && info.srcUrl.length) {
			openUrl(info.srcUrl, true, false);
		}
	} else if (info.menuItemId === imageReloadImageMemuItemID) {
		if (info.srcUrl && info.srcUrl.length) {
			// inject content script, send message with URL to reload image
			inject({action: "reload-image", payload: info.srcUrl});
		}
	} else if (info.menuItemId === imageImagePropertiesMenuItemID) {
		if (info.srcUrl && info.srcUrl.length) {
			// inject content script, send message with URL to display info
			inject({action: "inspect-image", payload: info.srcUrl});
		}
	} else if (info.menuItemId === selectionDictionaryMenuItemID) {
		if (info.selectionText && info.selectionText.length) {
			openUrl(DICTIONARY_URL + encodeURIComponent(info.selectionText), true, true);
		}
	} else if (info.menuItemId === selectionEncyclopediaMenuItemID) {
		if (info.selectionText && info.selectionText.length) {
			openUrl(ENCYCLOPEDIA_URL + encodeURIComponent(info.selectionText), true, true);
		}
	} else if (info.menuItemId === selectionTranslateMenuItemID) {
		if (info.selectionText && info.selectionText.length) {
			openUrl(TRANSLATE_URL + encodeURIComponent(info.selectionText), true, true);
		}
	} else if (info.parentMenuItemId === pageReloadParentMenuItemID) {
		onPageReloadSelected(info, tab);
	}
});
