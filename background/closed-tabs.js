chrome.runtime.onConnect.addListener(function(port) {
	if (port.name === "closed-tab-worker") {
		port.onMessage.addListener(function(message) {
			if (message.action && message.action.length) {
				if (message.action === "get-all") {
					chrome.storage.sync.get({closedTabs: []}, function(data) {
						port.postMessage({action: "closed-tabs", payload: data.closedTabs});
					});
				} else if (message.action === "remove-entry" && message.payload) {
					// remove item from storage: payload = url
					chrome.storage.sync.get({closedTabs: []}, function(data) {
						removeClosedTab(data.closedTabs, message.payload);
					});
				} else if (message.action === "remove-all") {
					chrome.storage.sync.set({closedTabs: []});
				}
			}
		});
	}
});

/* Runtime listeners */
chrome.runtime.onInstalled.addListener(function() {
    Console.log("installed");
    initializeTabList();
});

chrome.runtime.onStartup.addListener(function() {
    Console.log("startup");
    initializeTabList();
});

/* Tab event listeners */
chrome.tabs.onCreated.addListener(function(tab) {
	Console.log("creating: %O", tab);
	updateTabList(tab);
});

chrome.tabs.onUpdated.addListener(function(id, info, tab) {
	Console.log("updating: %O", tab);
	updateTabList(tab);
});

chrome.tabs.onRemoved.addListener(function(removedTabID, removeInfo) {
	markTabAsClosed(removedTabID);
});

chrome.tabs.onReplaced.addListener(function(addedTabID, removedTabID) {
	markTabAsClosed(removedTabID);
});

/* Closed tab functions */
function storeClosedTab(tab) {
	Console.log("checking last history entry: %O", tab);
	if (!/^https?:/.test(tab.url)) return;

	var closedTabInfo = {url: tab.url, title: tab.title, tabIndex: tab.index};

	// get existing closed tabs
	chrome.storage.sync.get({closedTabs: []}, function(data) {
		var indexOfCurrent = -1;
		// push new item if it's not in the list, limit to list of 50 for now

		if (data.closedTabs.length) {
			data.closedTabs.some(function(element, index) {
				if (element.url && element.url === closedTabInfo.url) {
					indexOfCurrent = index;
					return true;
				}
				return false;
			});

			if (indexOfCurrent === -1) {
				// add it
				data.closedTabs.splice(0, 0, closedTabInfo);
			} else {
				// bring existing element to top
				data.closedTabs.splice(indexOfCurrent, 1);
				data.closedTabs.splice(0, 0, closedTabInfo);
			}

			if (data.closedTabs.length > 50) {
				data.closedTabs.pop();
			}
		} else {
			// just add it
			data.closedTabs.splice(0, 0, closedTabInfo);
		}

		chrome.storage.sync.set({closedTabs: data.closedTabs});
	});
};

function removeClosedTab(closedTabs, url) {
	var indexOfCurrent = -1, foundElements = true;

	if (closedTabs.length) {
		while (foundElements) {
			foundElements = closedTabs.some(function(element, index) {
				if (element.url && element.url === url) {
					indexOfCurrent = index;
					return true;
				}
				return false;
			});

			if (foundElements && indexOfCurrent > -1) {
				closedTabs.splice(indexOfCurrent, 1);
			}
		}
	}

	chrome.storage.sync.set({closedTabs: closedTabs});
};

function updateTabList(tab) {
	var tabIndex = -1;
	// add to sync.closedTabs 
	// {url, title}
	chrome.storage.local.get({tabList: []}, function(data) {
		if (data.tabList.length) {
			data.tabList.some(function(value, index) {
				if (value.id === tab.id) {
					tabIndex = index; 
					return true;
				}
				return false;
			});
		}

		if (tabIndex > -1) {
			data.tabList.splice(tabIndex, 1, tab);
		} else {
			data.tabList.push(tab);
		}
		chrome.storage.local.set({tabList: data.tabList});
	});
};

function markTabAsClosed(tabID) {
	var tab = null, tabIndex = -1;
	// add to sync.closedTabs 
	// {url, title}
	chrome.storage.local.get({tabList: []}, function(data) {
		if (data.tabList.length) {
			data.tabList.some(function(value, index) {
				if (value.id === tabID) {
					tab = value;
					tabIndex = index; 
					return true;
				}
				return false;
			});
		}

		if (tab) {
			storeClosedTab(tab);
		}

		if (tabIndex > -1) {
			data.tabList.splice(tabIndex, 1);
			chrome.storage.local.set({tabList: data.tabList});
		}
	});
};

function initializeTabList() {
	chrome.tabs.query({}, function(tabs) {
		chrome.storage.local.set({tabList: tabs});
	});
};