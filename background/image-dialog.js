chrome.runtime.onConnect.addListener(function(port) {
	if (port.name === "image-worker") {
		port.onMessage.addListener(function(message) {
			if (message.action && message.action.length && message.payload) {
				if (message.action === "image-analyze") {
					ImageWorkerFactory.create(message.payload, function(event) {
						if (event.data.action && event.data.action.length && event.data.payload) {
							if (event.data.action === "complete" || event.data.action == "error") {
								port.disconnect();
							} else {
								port.postMessage({action: event.data.action, payload: event.data.payload});
							}
						}
					});
				}
			}
		});
	}
});

/* Factories */
var ImageWorkerFactory = (function() {

	function spawnWorker(url, callback) {
		var worker = new Worker("worker.js");
		worker.onmessage = callback;
		worker.postMessage({action: "image-analyze-start", payload: url});
	};

	return {
		create: spawnWorker
	};
})();