chrome.alarms.onAlarm.addListener(onReloadAlarm);

/* Tab event listeners */
chrome.tabs.onRemoved.addListener(function(removedTabID, removeInfo) {
	toggleReloadTimer(removedTabID, 0, true);
});

chrome.tabs.onReplaced.addListener(function(addedTabID, removedTabID) {
	toggleReloadTimer(removedTabID, 0, true);
});

chrome.tabs.onActivated.addListener(function(tabInfo) {
	updateReloadSelectedContextMenu(tabInfo.tabId);
});

/* Runtime listeners */
chrome.runtime.onInstalled.addListener(function() {
    Console.log("installed");
    initializeAlarms();
});

chrome.runtime.onStartup.addListener(function() {
    Console.log("startup");
    initializeAlarms();
});

/* Reload every */
function onPageReloadSelected(info, tab) {
	var interval = info.menuItemId.split("_").pop();
	toggleReloadTimer(tab.id, Number(interval), info.checked);
};

function toggleReloadTimer(tabID, interval, checked, fromInitialize) {
	var alarmKey = RELOAD_ALARM_KEY + tabID;
	if (interval === 0 || !checked) {
		chrome.storage.local.get(RELOAD_MAP_KEY, function(items) {
			// clear from storage
			var tabs = new Map(items[RELOAD_MAP_KEY]), setObject = {};
			if (tabs.has(tabID)) {
				tabs.delete(tabID);
				setObject[RELOAD_MAP_KEY] = Array.from(tabs);
				chrome.storage.local.set(setObject);
			}
			// remove alarm
			chrome.alarms.clear(alarmKey);
		});
		return;
	}

	// Loading from storage, set up alarm and exit
	if (fromInitialize) {
		chrome.alarms.create(alarmKey, {periodInMinutes: interval});
		return;
	}

	// update stored value
	chrome.storage.local.get(RELOAD_MAP_KEY, function(items) {
		// add to storage
		var tabs = new Map(items[RELOAD_MAP_KEY]), setObject = {};
		// early out if alarm exists with current interval
		if (tabs.has(tabID) && tabs.get(tabID) === interval) return;
		tabs.set(tabID, interval);
		setObject[RELOAD_MAP_KEY] = Array.from(tabs);
		chrome.storage.local.set(setObject);
		// set up alarm
		chrome.alarms.create(alarmKey, {periodInMinutes: interval});
	});
};

function updateReloadSelectedContextMenu(tabID) {
	// if in storage, set checked to correct context menu
	// otherwise, set checked to 0

	chrome.storage.local.get(RELOAD_MAP_KEY, function(items) {
		var tabs = new Map(items[RELOAD_MAP_KEY]);
		if (tabs.has(tabID)) {
			setReloadSelectedContextMenu(tabs.get(tabID));
			return;
		}

		setReloadSelectedContextMenu(0);
	});
};

function setReloadSelectedContextMenu(interval) {
	interval = "_" + interval;
	chrome.contextMenus.update(pageReloadEveryNone, {checked: pageReloadEveryNone.endsWith(interval)});
	chrome.contextMenus.update(pageReloadEveryOneMinute, {checked: pageReloadEveryOneMinute.endsWith(interval)});
	chrome.contextMenus.update(pageReloadEveryFiveMinute, {checked: pageReloadEveryFiveMinute.endsWith(interval)});
	chrome.contextMenus.update(pageReloadEveryThirtyMinute, {checked: pageReloadEveryThirtyMinute.endsWith(interval)});
	chrome.contextMenus.update(pageReloadEveryOneHour, {checked: pageReloadEveryOneHour.endsWith(interval)});
};

function initializeAlarms() {
	// start fresh
	chrome.alarms.clearAll(function() {
		
		chrome.storage.local.get(RELOAD_MAP_KEY, function(items) {
			var tabs = new Map(items[RELOAD_MAP_KEY]);

			// set up existing alarms
			tabs.forEach(function(value, key) {
				toggleReloadTimer(key, value, true, true);
			});

			// early exit if no entries
			if (!tabs.size) {
				setReloadSelectedContextMenu(0);
				return;
			}

			// select menu item for this tab
			chrome.tabs.query({active: true, currentWindow: true}, function(activeTabs) {
				if (!activeTabs.length) return;
				var tab = activeTabs[0];

				if (tabs.has(tab.id)) {
					setReloadSelectedContextMenu(tabs.get(tab.id));
					return;
				}
			});
		});
	});
};

function onReloadAlarm(alarm) {
	if (!alarm.name.startsWith(RELOAD_ALARM_KEY) || !/_\d+$/.test(alarm.name)) return;

	var tabID = Number(alarm.name.split("_").pop());
	chrome.tabs.reload(tabID);
};
