# README

Opera 15+ extension to bring back some missing context menu and bookmark functionality.
Includes context menu items for page, link, image, and frame elements. It also includes a browser button to act as a lightweight bookmark manager.

Closed tab manager uses chrome.sync storage area, so when Opera enables that feature they will track across your browser sessions.

Feel free to contact me or add an issue for a bug or feature you want.

Check out and load as an unpacked extension for Opera 15+.

## CHANGELOG

v1.3: Convert injected script to use HTML5 dialog element. Adds "reload every..." function.

v1.2: Add nice output of image size.

v1.1.1: Fix inconsistent context menu items.

v1.1: Adds closed tabs to browser action popup, Opera code style compliancy fix.

v1.0: Initial Release


## CREDITS

[ExifReader](https://github.com/mattiasw/ExifReader) by Mattias Wallander

[png.js](https://github.com/devongovett/png.js/) by Devon Govett

[Font Awesome](http://fortawesome.github.io/Font-Awesome/) by Dave Gandy

[ClassicImages](https://github.com/ChaosinaCan/ClassicImages) by Joel Spadin for inspiration around the image dialogs


## LICENSE

Copyright 2015 Matt Murphy

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.