/* Factories */
var RequestFactory = (function() {
	function request(url) {
		return new Promise(function(resolve, reject) {
			var req = new XMLHttpRequest();
			req.responseType = "blob";
			req.open("GET", url, true);
			req.onload = function(event) {
				// This is called even on 404 etc
				// so check the status
				if (req.status == 200) {
					// Resolve the promise with the response
					resolve(req.response);
				} else {
					// Otherwise reject with the status text
					// which will hopefully be a meaningful error
					reject(Error(req.statusText));
				}
			};
			req.send(null);
		});
	};

	return {
		get: request
	};
})();

/* Message handling */
onmessage = function(message) {
	var data = message.data;
	console.log(data);
	if (data.action && data.action.length && data.payload && data.payload.length) {
		if (data.action === "image-analyze-start") {
			parseImage(data.payload);
		}
	}
};

function sendImageData(action, payload) {
	postMessage({action: action, payload: payload});
	if (action === "error" || action === "complete") {
		self.close();
	}
};

/* Utility functions */
function parseImage(url) {
	Promise.resolve()
		.then(function() {
			return RequestFactory.get(url);
		})
		.then(function(response) {
			var fileReader = new FileReader();
			fileReader.addEventListener("load", function(event) {
				var basicData = getBasicImageData(response.type, fileReader.result);

				sendImageData("basic-data", basicData);

				try {
					if (basicData.imageType === "JPEG") {
						parseJpeg(basicData, fileReader.result);
					} else if (basicData.imageType === "PNG") {
						parsePng(basicData, fileReader.result);
					}
				} catch (error) {
					sendImageData("error", error.toString());
				}
				sendImageData("complete", "");
			});

			fileReader.addEventListener("error", function(event){
				sendImageData("error", event.error.toString());
				sendImageData("complete", "");
			});

			fileReader.readAsArrayBuffer(response);
		});
};

function getBasicImageData(type, bytes) {
	var mimeTypes = [
			{key: "BMP", values: ["image/bmp", "image/x-windows-bmp"]},
			{key: "GIF", values: ["image/gif"]},
			{key: "ICO", values: ["image/x-icon"]},
			{key: "JPEG", values: ["image/jpeg", "image/pjpeg"]},
			{key: "PGM", values: ["image/x-portable-graymap"]},
			{key: "PNG", values: ["image/png"]},
			{key: "SVG", values: ["image/svg+xml", "image/svg-xml"]},
			{key: "TIFF", values: ["image/tiff", "image/x-tiff"]},
			{key: "WebP", values: ["image/webp"]},
			{key: "XBM", values: ["image/x-xbitmap"]}
		], mimeType;

	mimeTypes.forEach(function(value, index) {
		if (value.values.indexOf(type.toLowerCase()) >= 0) {
			mimeType = value.key;
		}
	});

	return {
		fileSize: bytes.byteLength,
		imageType: mimeType
	};
};

function parseJpeg(data, bytes) {
	importScripts("lib/ExifReader.js");

	var exif = new ExifReader();
	exif.load(bytes);
	console.log(exif.getAllTags());
	sendImageData("exif", exif.getAllTags());
};

function parsePng(data, bytes) {
	importScripts("lib/zlib.js");
	importScripts("lib/png.js");
	var info = {},
		byteArray = new Uint8Array(bytes),
		png = new PNG(byteArray);

	info.bitsPerPixel = png.pixelBitlength;

	if (png.animation) {
		info.frameCount = png.animation.numFrames;
		info.duration = 0;
		png.animation.frames.forEach(function(value, index) {
			info.duration += value.delay;
		});
		info.duration /= 1000;
		info.frameRate = info.frames / info.duration;
	}

	for (var item in png.text) {
		if (png.text.hasOwnProperty(item)) {
			info[item] = {description: png.text[item]};
		}
	}
	console.log(info);
	sendImageData("png-data", info);
};
