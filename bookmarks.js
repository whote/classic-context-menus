var ConnectionFactory = (function() {
	var bookmarkWorker = null,
		closedTabWorker = null;

	function initializeBookmarkWorker() {
		bookmarkWorker = chrome.runtime.connect({name: "bookmark-worker"});
		return bookmarkWorker;
	};

	function initializeClosedTabWorker() {
		closedTabWorker = chrome.runtime.connect({name: "closed-tab-worker"});
		return closedTabWorker;
	};

	return {
		bookmarkWorker: function() {return bookmarkWorker || initializeBookmarkWorker();},
		closedTabWorker: function() {return closedTabWorker || initializeClosedTabWorker();}
	};
})();

function onDocumentLoad() {
	var bookmarkWorker = ConnectionFactory.bookmarkWorker(),
		closedTabWorker = ConnectionFactory.closedTabWorker();

	bookmarkWorker.postMessage({action: "get-all", payload: null});
	bookmarkWorker.onMessage.addListener(function(event) {
		if (event.action && event.action.length) {
			if (event.action === "bookmarks" && event.payload) {
				if (event.payload.length) {
					displayBookmarks(event.payload[0]);
				}
			}
		}
	});

	closedTabWorker.postMessage({action: "get-all", payload: null});
	closedTabWorker.onMessage.addListener(function(event) {
		if (event.action && event.action.length) {
			if (event.action === "closed-tabs" && event.payload) {
				if (event.payload.length) {
					displayClosedTabs(event.payload);
				}
			}
		}
	});

	window.onunload = function() {
		bookmarkWorker.disconnect();
		closedTabWorker.disconnect();
	};
};

function displayClosedTabs(tabs) {
	document.getElementById("closed-tabs-count").textContent = tabs.length || "0";
	document.getElementById("toggle-closed-tabs").addEventListener("click", function(event) {
		var element = event.target,
			icon = element.querySelector("i"),
			list = document.getElementById("closed-tabs");

		if (list.style.display === "none") {
			// show
			icon.className = "fa fa-folder-open";
			list.style.display = "block";
		} else {
			// hide
			icon.className = "fa fa-folder";
			list.style.display = "none";
		}
	}, false);
	document.getElementById("remove-closed-tabs").addEventListener("click", function(event) {
		var worker = ConnectionFactory.closedTabWorker(),
			list = document.getElementById("closed-tabs");

		event.stopImmediatePropagation();

		while (list.firstChild) {
			list.removeChild(list.firstChild);
		}
		document.getElementById("closed-tabs-count").textContent = "0";
		worker.postMessage({action: "remove-all", payload: null});
	}, false);

	tabs.forEach(function(value, index) {
		var item = document.createElement("li"),
			link = document.createElement("a"),
			favicon = document.createElement("img"),
			deleteLink = document.createElement("span");

		link.textContent = value.title;
		link.title = value.title;
		link.href = value.url;
		link.className = "bookmark";
		link.dataset.index = value.tabIndex;
		link.addEventListener("click", openClosedTab, false);

		favicon.className = "favicon";
		favicon.src = "opera://favicon/" + value.url;

		deleteLink.addEventListener("click", deleteClosedTabEntry, false);
		deleteLink.className = "delete-bookmark";
		deleteLink.dataset.url = value.url;
		deleteLink.innerHTML = "<i class='fa fa-times-circle'></i>";

		link.appendChild(deleteLink);
		link.appendChild(favicon);
		item.appendChild(link);
		document.getElementById("closed-tabs").appendChild(item);
	});
};

function deleteClosedTabEntry(event) {
	var worker = ConnectionFactory.closedTabWorker(),
		element = event.target.parentElement;

	event.stopImmediatePropagation();
	worker.postMessage({action: "remove-entry", payload: element.dataset.url});
	element.parentElement.parentElement.style.display = "none";
};

function openClosedTab(event) {
	var worker, element = event.target;
	if (element.href) {
		chrome.tabs.create({url: element.href, active: true, index: parseInt(element.dataset.index, 10)});
		worker = ConnectionFactory.closedTabWorker();
		worker.postMessage({action: "remove-entry", payload: element.href});
	}
};

function displayBookmarks(tree) {
	var elements = getElementsFromChildren(tree.children),
		list = document.querySelector("ul.bookmark-view"),
		element, link;

	clearList(list);

	if (tree.id !== "0") {
		element = document.createElement("li");
		element.className = "open-folder";
		link = document.createElement("a");
		link.id = tree.parentId;
		link.innerHTML = "<i class='fa fa-folder-open'></i>" + tree.title;
		link.addEventListener("click", openBookmark, false);
		element.appendChild(link);
		list.appendChild(element);
	}

	elements.forEach(function(value, index) {
		list.appendChild(value);
	});

	if (tree.id === "0") {
		// top level no indent
		for (var i = 0; i < list.children.length; i++) {
			list.children[i].className += "open-folder";
		}
	}
};

function clearList(list) {
	while (list.firstChild) {
		list.removeChild(list.firstChild);
	}
};

function getElementsFromChildren(subTree) {
	var elements = [];
	subTree.forEach(function(value, index) {
		var element = document.createElement("li"),
			link = document.createElement("a"),
			deleteLink = null, favicon = null;

		link.textContent = value.title;
		link.title = value.title;
		if (value.children) {
			// its a folder
			link.innerHTML = "<i class='fa fa-folder'></i>" + value.title;
			link.className = "folder";
		} else {
			// its a bookmark
			link.href = value.url;
			link.className = "bookmark";
			deleteLink = document.createElement("span");
			deleteLink.addEventListener("click", deleteBookmark, false);
			deleteLink.className = "delete-bookmark";
			deleteLink.dataset.bookmarkId = value.id;
			deleteLink.innerHTML = "<i class='fa fa-times-circle'></i>";
			link.appendChild(deleteLink);

			favicon = document.createElement("img");
			favicon.className = "favicon";
			favicon.src = "opera://favicon/" + link.href;
			link.appendChild(favicon);
		}
		link.addEventListener("click", openBookmark, false);
		link.id = value.id;
		element.appendChild(link);
		elements.push(element);
	});

	return elements;
};

function openBookmark(event) {
	var worker, element = event.target;
	if (element.href) {
		chrome.tabs.update(null, {url: element.href, active: true});
	} else {
		worker = ConnectionFactory.bookmarkWorker();
		worker.postMessage({action: "get-all", payload: element.id});
	}
};

function deleteBookmark(event) {
	var worker = ConnectionFactory.bookmarkWorker(),
		element = event.target.parentElement;

	event.stopImmediatePropagation();
	worker.postMessage({action: "delete-bookmark", payload: element.dataset.bookmarkId});
	element.parentElement.parentElement.style.display = "none";
};

document.body.onload = onDocumentLoad;